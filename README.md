Log Generator
=====================

How to run :

```mvn spring-boot:run```

How to package:

```mvn package```

How to use:

```java -jar dist/log-generator-0.0.1-SNAPSHOT.jar```
