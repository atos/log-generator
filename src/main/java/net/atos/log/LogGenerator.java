package net.atos.log;

import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Created by Paweł Antczak on 2015-06-02.
 */
@Component
public class LogGenerator {

    Logger log = Logger.getLogger(LogGenerator.class);

    @Scheduled(cron = "${log.cron}")
    public void evaluate() {
        log.trace("Trace log.");
        log.debug("Debug log.");
        log.info("Info log.");
        log.warn("Warn log.");
        log.error("Error log.");
        log.fatal("Fatal log.");
    }
}
